package com.voicefeeds;

/**
 * Created by W&L on 30/10/2014.
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.voicefeeds.beans.NewSaved;
import com.voicefeeds.utils.MySQLiteHelper;

public class NewSavedDatasource {


    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_TITLE,
            MySQLiteHelper.COLUMN_DESCRIPTION,
            MySQLiteHelper.COLUMN_LINK,
            MySQLiteHelper.COLUMN_DATEPUB};

    private Date dateAux;

    private Context contextObj;


    public NewSavedDatasource(Context context) {
        contextObj = context;
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public NewSaved createNew(String title, String description, String link, String fechaPub) {
        ContentValues values = new ContentValues();

        values.put(MySQLiteHelper.COLUMN_TITLE, title);
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, description);
        values.put(MySQLiteHelper.COLUMN_LINK, link);
        values.put(MySQLiteHelper.COLUMN_DATEPUB, fechaPub);

        long insertId = database.insert(MySQLiteHelper.TABLE_NEWS, null,
                values);

        Cursor cursor = database.query(MySQLiteHelper.TABLE_NEWS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);

        cursor.moveToFirst();
        NewSaved newComment = cursorToNew(cursor);
        cursor.close();

        return newComment;
    }

    public void deleteComment(NewSaved comment) {
        //long id = comment.getId();
        String link = comment.getLink();

        //System.out.println("Comment deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_NEWS, MySQLiteHelper.COLUMN_LINK
                + " = '" + link+"'", null);
    }

    public List<NewSaved> getAllNews() {
        List<NewSaved> newsList = new ArrayList<NewSaved>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_NEWS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            NewSaved newObj = cursorToNew(cursor);
            newsList.add(newObj);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return newsList;
    }

    private NewSaved cursorToNew(Cursor cursor) {

        try {

            NewSaved comment = new NewSaved();
            comment.setId(cursor.getLong(0));
            comment.setName(cursor.getString(1));
            comment.setDescription(cursor.getString(2));
            comment.setLink(cursor.getString(3));

            if (!cursor.getString(4).equals("")){

                dateAux = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy", Locale.US).parse(cursor.getString(4));
                TimeAgo timeObj = new TimeAgo(contextObj);
                if (dateAux != null) comment.setDatePub(timeObj.timeAgo(dateAux.getTime()));
                else comment.setDatePub("");
            }

            return comment;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
