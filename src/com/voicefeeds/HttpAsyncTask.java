package com.voicefeeds;

import android.os.AsyncTask;
import android.util.Log;
import com.voicefeeds.beans.HttpVFResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by W&L on 28/10/2014.
 */
public class HttpAsyncTask extends AsyncTask<List, Void, HttpVFResponse> {

    /**
     * HttpResponse object to get the server response
     */
    private HttpResponse response;

    /**
     * HttpPost object to make post request
     */
    private HttpPost httpPost;

    /**
     * HttpClient object to configure http requests
     */
    private HttpClient httpClient;

    /**
     * String attribute to get the string response of the request
     */
    private String responseBody="";

    /**
     * HttpClient entity to set parameters to be passed to the request to be done
     */
    private HttpEntity entityResponse;

    /**
     * url address to use to make the http request
     */
    private String urlAddr = "";


    public HttpAsyncTask(String url) {
        urlAddr = url;
    }


    /**
     * This function is going to be executed when the instance invokes the
     * execute() method
     *
     * @param params
     */
    @Override
    protected HttpVFResponse doInBackground(List... params) {
        return makePostRequest(params);
    }

    /**
     * Method that makes the http request into the server
     * @param params
     * @return HttpVFResponse
     */
    public HttpVFResponse makePostRequest(List[] params) {

        List aux = params[0];
        httpClient = new DefaultHttpClient();
        // replace with your url
        httpPost = new HttpPost(urlAddr);

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(aux));
        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }

        //making POST request.
        try {
            response = httpClient.execute(httpPost);
            entityResponse = response.getEntity();
            responseBody = EntityUtils.toString(entityResponse);
            // write response to log
            Log.d("Http Post Response:", response.toString());
        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }

        return new HttpVFResponse(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseBody);
    }

}
