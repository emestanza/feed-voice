package com.voicefeeds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Iterator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.voicefeeds.beans.NewSaved;


public class NoticiasAdapter extends ArrayAdapter<Noticia> {

    /**
     * ArrayList which has the news to be processed by the adapter
     */
    private ArrayList<Noticia> noticias;

    /**
     * Context to be used by the adapter
     */
    private Context context;

    /**
     * array to add the color integer numbers
     */
    public static int[] arrayColor;

    /**
     * Noticia object to use in the adapter
     */
    private Noticia noticia;

    /**
     * NewSavedDatasource object to make queries
     */
    private NewSavedDatasource dataSource;

    /**
     * List to get the NewSaved objects to be processed by the adapter
     */
    private List<NewSaved> newsList;

    public NoticiasAdapter(Context context, int textViewResourceId, ArrayList<Noticia> noticias, List<NewSaved> newList) {
        super(context, textViewResourceId, noticias);
        this.noticias = noticias;
        this.context = context;
        this.arrayColor = new int[]{context.getResources().getColor(R.color.voice_feed_green),
                context.getResources().getColor(R.color.voice_feed_blue),
                context.getResources().getColor(R.color.voice_feed_purple),
                context.getResources().getColor(R.color.voice_feed_pink),
                context.getResources().getColor(R.color.voice_feed_orange)};

        this.dataSource = new NewSavedDatasource(context);
        this.dataSource.open();
        this.newsList = newList;
    }

    /*
    *  Static class to help to process the event data with the adapter object
    */
    static class ViewHolder {

        public LinearLayout channelLayout;
        public TextView noticiaTexto;
        public TextView timeTxt;
        public ImageView webViewImg;
        public int position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.i("POSITION", "POSITION = "+position);

        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.fila, null);
            viewHolder.noticiaTexto = (TextView) rowView.findViewById(R.id.FilaTexto1);
            viewHolder.webViewImg = (ImageView) rowView.findViewById(R.id.webViewImg);
            viewHolder.timeTxt = (TextView) rowView.findViewById(R.id.newTimeTxt);
            viewHolder.position = position;
            rowView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) rowView.getTag();
        }

        LinearLayout channelLayout = (LinearLayout) rowView.findViewById(R.id.rowNewLayout);
        channelLayout.setBackgroundColor(arrayColor[getArrayIndex(position)]);
        ImageView saveNewImg = (ImageView) rowView.findViewById(R.id.saveNewImg);

        noticia = noticias.get(position);

        if (noticia != null) {

            Date auxDate = null;
            try {
                auxDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).parse(noticia.getFecha());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            TimeAgo timeObj = new TimeAgo(context);
            if (auxDate != null) viewHolder.timeTxt.setText(timeObj.timeAgo(auxDate.getTime()));
            else viewHolder.timeTxt.setText("");

            viewHolder.webViewImg.setOnClickListener(new imageViewClickListener(position) {

                @Override
                public void onClick(View v) {
                    Noticia noticiaObj = noticias.get(this.position);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(noticiaObj.getLink()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            Iterator itr = newsList.iterator();
            boolean passed = false;
            while(itr.hasNext()) {
                NewSaved auxSave= (NewSaved)itr.next();

                if (auxSave != null){
                    if(auxSave.getLink().equals(noticia.getLink())){
                        passed = true;
                        break;
                    }
                }
            }

            if (passed){
                saveNewImg.setImageResource(R.drawable.ic_launcher_savefill);
                saveNewImg.setTag(R.drawable.ic_launcher_savefill);
            }
            else {
                saveNewImg.setImageResource(R.drawable.ic_launcher_notsave);
                saveNewImg.setTag(R.drawable.ic_launcher_notsave);
            }

            saveNewImg.setOnClickListener(new imageViewClickListener(position) {

                @Override
                public void onClick(View v) {
                    Noticia noticiaObj = noticias.get(this.position);

                    ImageView aux = (ImageView) v.findViewById(R.id.saveNewImg);

                    if (aux.getTag().toString().contains("ic_launcher_notsave") || aux.getTag().toString().contains("2130837610")) {
                        aux.setImageResource(R.drawable.ic_launcher_savefill);
                        aux.setTag(R.drawable.ic_launcher_savefill);

                        Date auxDate = null;
                        try {

                            if(noticiaObj.getFecha().equals("") )
                                dataSource.createNew(noticiaObj.getTitulo(), noticiaObj.getDescripcion().toString(), noticiaObj.getLink(), "");
                            else{
                                auxDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).parse(noticiaObj.getFecha());
                                dataSource.createNew(noticiaObj.getTitulo(), noticiaObj.getDescripcion().toString(), noticiaObj.getLink(), auxDate.toString());
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    } else {
                        aux.setImageResource(R.drawable.ic_launcher_notsave);
                        aux.setTag(R.drawable.ic_launcher_notsave);
                        NewSaved newDelete = new NewSaved();
                        newDelete.setLink(noticiaObj.getLink());
                        dataSource.deleteComment(newDelete);
                    }
                }
            });

            if (viewHolder.noticiaTexto != null) {
                viewHolder.noticiaTexto.setText(noticia.getTitulo());
            }

        }

        return rowView;
    }

    private class imageViewClickListener implements View.OnClickListener {
        int position;

        public imageViewClickListener(int pos) {
            this.position = pos;
        }

        public void onClick(View v) {

        }
    }


    public int getArrayIndex(int index) {
        int res = index;

        if (index <= 4) {
            return res;
        } else {
            return getArrayIndex(res - 5);
        }

    }

}
