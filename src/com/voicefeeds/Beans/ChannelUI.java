package com.voicefeeds.beans;

import java.io.Serializable;

/**
 * Created by Dustin J on 18/06/2014.
 */
public class ChannelUI implements Serializable {

    private String channelId;
    private int channelDrawable;
    private String channelName;
    private String channelImage;
    private String[] channelCategories;
    private String[] channelUrls;
    private int categoryIndexSelected;
    private String dateSelected;
    private boolean isTTSTested;

    public ChannelUI(String id, String channelName, String channelImage, String[] channelCategories) {
        this.setChannelId(id);
        this.channelImage = channelImage;
        this.channelName = channelName;
        this.channelCategories = channelCategories;
        this.setTTSTested(false);
    }

    public int getChannelDrawable() {
        return channelDrawable;
    }

    public void setChannelDrawable(int channelDrawable) {
        this.channelDrawable = channelDrawable;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String[] getChannelCategories() {
        return channelCategories;
    }

    public void setChannelCategories(String[] channelCategories) {
        this.channelCategories = channelCategories;
    }

    public String getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(String dateSelected) {
        this.dateSelected = dateSelected;
    }

    public int getCategoryIndexSelected() {
        return categoryIndexSelected;
    }

    public void setCategoryIndexSelected(int categoryIndexSelected) {
        this.categoryIndexSelected = categoryIndexSelected;
    }

    public boolean isTTSTested() {
        return isTTSTested;
    }

    public void setTTSTested(boolean isTTSTested) {
        this.isTTSTested = isTTSTested;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String[] getChannelUrls() {
        return channelUrls;
    }

    public void setChannelUrls(String[] channelUrls) {
        this.channelUrls = channelUrls;
    }

    public String getChannelImage() {
        return channelImage;
    }

    public void setChannelImage(String channelImage) {
        this.channelImage = channelImage;
    }
}
