package com.voicefeeds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.voicefeeds.beans.ChannelUI;
import com.voicefeeds.adapters.ChannelListAdapter;
import com.voicefeeds.beans.HttpVFResponse;
import com.voicefeeds.utils.AppConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by Dustin J on 18/06/2014.
 */
public class ChannelListActivity extends ActionBarActivity {

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * ListView attribute to manage the channel list
     */
    private ListView channelList;

    /**
     * Adapter to be applied to channel list
     */
    private ChannelListAdapter channelAdapter;

    /**
     * onCreate method override
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_list_activity);
        initElements();
    }

    /**
     * Initialize all the elements to be used by the activity
     */
    public void initElements() {

        actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.selectChannelStr));
        channelList = (ListView) findViewById(R.id.channelList);
        HttpAsyncTask requestObj = new HttpAsyncTask(AppConfig.GET_NEWSPAPERS_URL);

        try {
            HttpVFResponse response = requestObj.execute(AppConfig.getMainParameters()).get();
            JSONArray json = new JSONArray(response.getResponseBody());
            ChannelUI[] channelUIList = new ChannelUI[json.length()];

            for (int i = 0; i < json.length(); i++) {

                JSONObject jsonObject = json.getJSONObject(i);
                String id = jsonObject.getString("id");
                String name = jsonObject.getString("nombre");
                String image = jsonObject.getString("imagen");
                channelUIList[i] = new ChannelUI(id, name, image, new String[0]);

            }

            channelAdapter = new ChannelListAdapter(this, channelUIList);
            channelList.setAdapter(channelAdapter);
            channelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                    ChannelUI channelObj = (ChannelUI) channelList.getItemAtPosition(position);
                    Intent intentObj = new Intent(ChannelListActivity.this, CategoryActivity.class);
                    intentObj.putExtra("channelObj", channelObj);
                    startActivity(intentObj);

                }

            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}