package com.voicefeeds.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.voicefeeds.NewSavedDatasource;
import com.voicefeeds.R;
import com.voicefeeds.beans.NewSaved;

import java.util.List;

/**
 * Created by Dustin J on 18/06/2014.
 */
public class SavedNewsListAdapter extends ArrayAdapter<NewSaved> {

    /**
     * Activity object to interact with the adapter
     */
    private Activity currentAct;

    /**
     * NewSaved list object to set into the adapter
     */
    private final List<NewSaved> newsObjList;

    /**
     * NewSaved list object to set into the adapter
     */
    public static int[] arrayColor;

    /**
     * Context object to get reference of it in the adapter
     */
    private Context contextObj;

    /**
     * NewSaved object to get an object by id
     */
    private NewSaved noticia;

    /**
     * NewSavedDatasource object to do some queries on sqlite db
     */
    private NewSavedDatasource dataSource;

    /**
     * NewSaved object to get an object by id
     */
    private NewSaved noticiaObj;

    public SavedNewsListAdapter(Activity act, Context context, List<NewSaved> channelList) {
        super(context, R.layout.savednew_row_layout, channelList);
        contextObj = context;
        newsObjList = channelList;
        currentAct = act;

        this.arrayColor = new int[]{context.getResources().getColor(R.color.voice_feed_green),
                context.getResources().getColor(R.color.voice_feed_blue),
                context.getResources().getColor(R.color.voice_feed_purple),
                context.getResources().getColor(R.color.voice_feed_pink),
                context.getResources().getColor(R.color.voice_feed_orange)};

    }

    /*
    *  Static class to help to process the event data with the adapter object
    */
    static class ViewHolder {
        //public LinearLayout channelLayout;
        public TextView savedNewTitle;
        public TextView savedNewTimeAgo;
        public ImageView deleteIcon;
        public ImageView webIcon;
    }


    /**
     * The ListView instance calls the getView() method on the adapter for each
     * data element. In this method the adapter creates the row layout and maps
     * the data to the views in the layout.
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (rowView == null) {

            LayoutInflater inflater = (LayoutInflater) contextObj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.savednew_row_layout, null);

            viewHolder.savedNewTitle = (TextView) rowView.findViewById(R.id.savedTitleTxt);
            viewHolder.savedNewTimeAgo = (TextView) rowView.findViewById(R.id.savedTimeTxt);
            viewHolder.webIcon = (ImageView) rowView.findViewById(R.id.savedWebViewImg);
            viewHolder.deleteIcon = (ImageView) rowView.findViewById(R.id.deleteNewIcon);

            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        noticia = newsObjList.get(position);

        if (noticia != null) {

            LinearLayout channelLayout = (LinearLayout) rowView.findViewById(R.id.rowSavedNewLayout);
            channelLayout.setBackgroundColor(arrayColor[getArrayIndex(position)]);

            viewHolder.savedNewTimeAgo.setText(noticia.getDatePub());
            viewHolder.savedNewTitle.setText(noticia.getName());
            viewHolder.webIcon.setOnClickListener(new imageViewClickListener(position) {

                @Override
                public void onClick(View v) {

                    noticiaObj = newsObjList.get(this.position);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(noticiaObj.getLink()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    contextObj.startActivity(intent);
                }
            });

            viewHolder.deleteIcon.setOnClickListener(new imageViewClickListener(position) {

                @Override
                public void onClick(View v) {

                    noticiaObj = newsObjList.get(this.position);

                    new AlertDialog.Builder(currentAct)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Confirmar Eliminación")
                            .setMessage("¿Está seguro de querer eliminar esta noticia de la lista de guardados?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dataSource = new NewSavedDatasource(contextObj);
                                    dataSource.open();
                                    dataSource.deleteComment(noticiaObj);
                                    newsObjList.remove(position);
                                    notifyDataSetChanged();
                                }

                            })
                            .setNegativeButton("NO", null)
                            .show();
                }
            });

        }

        return rowView;
    }

    /**
     * Method to get a color integer by index
     *
     * @param index
     * @return int
     */
    public int getArrayIndex(int index) {
        int res = index;

        if (index <= 4) {
            return res;
        } else {
            return getArrayIndex(res - 5);
        }

    }

    /**
     * Private class to implement a click event into the adapter
     */
    private class imageViewClickListener implements View.OnClickListener {
        int position;

        public imageViewClickListener(int pos) {
            this.position = pos;
        }

        public void onClick(View v) {

        }
    }

}