package com.voicefeeds.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;
import com.voicefeeds.LoadImage;
import com.voicefeeds.beans.ChannelUI;
import com.voicefeeds.R;
import com.voicefeeds.utils.AppConfig;

import java.util.concurrent.ExecutionException;

/**
 * Created by Dustin J on 18/06/2014.
 */
public class ChannelListAdapter extends ArrayAdapter<ChannelUI> {

    /**
     * Activity object to interact with the adapter
     */
    private final Activity channelContext;

    /**
     * ChannelUI object array to set into the adapter
     */
    private final ChannelUI[] channelObjList;

    /**
     * Bitmap object to get newspaper image
     */
    private Bitmap imageBitmap;

    /**
     * LoadImage object to request imagen from server
     */
    private LoadImage loadImgObj;

    public ChannelListAdapter(Activity context, ChannelUI[] channelList) {
        super(context, R.layout.channel_row_layout, channelList);
        channelContext = context;
        channelObjList = channelList;
    }

    /*
    *  Static class to help to process the event data with the adapter object
    */
    static class ViewHolder {
        public LinearLayout channelLayout;
        public TextView channelName;
        public ImageView channelDrawable;
    }


    /**
     * The ListView instance calls the getView() method on the adapter for each
     * data element. In this method the adapter creates the row layout and maps
     * the data to the views in the layout.
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (rowView == null) {

            LayoutInflater inflater = channelContext.getLayoutInflater();
            rowView = inflater.inflate(R.layout.channel_row_layout, null);
            viewHolder.channelName = (TextView) rowView.findViewById(R.id.channelNameTxtView);
            viewHolder.channelDrawable = (ImageView) rowView.findViewById(R.id.channelImg);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        ChannelUI channelUIObj = channelObjList[position];

        try {
            loadImgObj = new LoadImage();
            imageBitmap = loadImgObj.execute(AppConfig.NEWSPAPER_DIRECTORY + "/" + channelUIObj.getChannelImage()).get();
            viewHolder.channelDrawable.setImageBitmap(imageBitmap);
            viewHolder.channelName.setText(channelUIObj.getChannelName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return rowView;
    }

}