package com.voicefeeds.utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W&L on 28/10/2014.
 */
public class AppConfig {

    public static final String BASE_API_URL= "http://voicefeeds.260mb.net";
    public static final String GET_NEWSPAPERS_URL = BASE_API_URL+"/api/getnewspaper";
    public static final String GET_CATEGORIES_URL = BASE_API_URL+"/api/getcategories";
    public static final String API_TOKEN = "skmGd325a28ss8HpFF5BUT89J4E6rE";
    public static final String NEWSPAPER_DIRECTORY = BASE_API_URL+"/img/newspaper";

    /**
     * This method returns a List with some parameters to be used in a http
     * request
     */
    public static List<NameValuePair> getMainParameters() {
        List parameters = new ArrayList();
        parameters.add(new BasicNameValuePair("token", API_TOKEN));

        return parameters;
    }

}
