package com.voicefeeds;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import com.voicefeeds.beans.ChannelUI;
import com.voicefeeds.beans.HttpVFResponse;
import com.voicefeeds.utils.AppConfig;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Dustin J on 18/06/2014.
 */
public class CategoryActivity extends ActionBarActivity {

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * Spinner to show categories
     */
    private Spinner categorySpinner;

    /**
     * ImageView object to set the newspaper image which was selected
     */
    private ImageView channelSelectedImg;

    /**
     * TextView object to set the newspaper name which was selected
     */
    private TextView channelSelectedName;

    /**
     * ChannelUI to be used by Intent
     */
    private ChannelUI channelUIObj;

    /**
     * Bitmap object to be used to get the newspaper image via request
     */
    private Bitmap imageBitmap;

    /**
     * LoadImage object to request via http
     */
    private LoadImage loadImgObj;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);

        initElements();
    }

    /**
     * Initialize all the elements to be used by the activity
     */
    @SuppressLint("NewApi")
    public void initElements() {

        actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.selectCategoryDateStr));
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        channelUIObj = (ChannelUI) getIntent().getSerializableExtra("channelObj");

        channelSelectedName = (TextView) findViewById(R.id.channelSelectedName);
        channelSelectedName.setText(channelUIObj.getChannelName());
        channelSelectedImg = (ImageView) findViewById(R.id.channelSelectedImg);

        try {
            loadImgObj = new LoadImage();
            imageBitmap = loadImgObj.execute(AppConfig.NEWSPAPER_DIRECTORY + "/" + channelUIObj.getChannelImage()).get();
            channelSelectedImg.setImageBitmap(imageBitmap);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (channelUIObj.getChannelCategories().length > 0) {
            categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, channelUIObj.getChannelCategories());
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(spinnerArrayAdapter);
        } else {

            HttpAsyncTask requestObj = new HttpAsyncTask(AppConfig.GET_CATEGORIES_URL);

            List<NameValuePair> parameters = AppConfig.getMainParameters();
            parameters.add(new BasicNameValuePair("id", channelUIObj.getChannelId()));

            try {
                HttpVFResponse response = requestObj.execute(parameters).get();

                JSONArray json = new JSONArray(response.getResponseBody());
                String[] categories = new String[json.length()];
                String[] urls = new String[json.length()];

                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = json.getJSONObject(i);
                    categories[i] = jsonObject.getString("nombre");
                    urls[i] = jsonObject.getString("rss");
                }

                channelUIObj.setChannelCategories(categories);
                channelUIObj.setChannelUrls(urls);

                categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, channelUIObj.getChannelCategories());
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(spinnerArrayAdapter);


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.category_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /*
    * Method called when item menu is selected
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent intent = null;

        switch (item.getItemId()) {
            case R.id.acceptCategoryBtn:
                intent = new Intent(CategoryActivity.this, LectorNoticias.class);
                channelUIObj.setCategoryIndexSelected(categorySpinner.getSelectedItemPosition());
                intent.putExtra("channelObj", channelUIObj);
                startActivity(intent);

                return true;

            case android.R.id.home:
                intent = new Intent(CategoryActivity.this, ChannelListActivity.class);
                startActivity(intent);

                return true;
        }

        return true;
    }

}