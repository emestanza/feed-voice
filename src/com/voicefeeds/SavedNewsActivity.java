package com.voicefeeds;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.voicefeeds.adapters.SavedNewsListAdapter;
import com.voicefeeds.beans.ChannelUI;
import com.voicefeeds.beans.NewSaved;
import java.util.List;

/**
 * Created by W&L on 04/11/2014.
 */
public class SavedNewsActivity extends ActionBarActivity {

    /**
     * Speech object to set the new spoken
     */
    private Speech speech;

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * ChannelUI object to get the previous values selected by the user
     */
    private ChannelUI channelUIObj;


    /**
     * application context to be used during the activity
     */
    private Context context;

    /**
     * NewSavedDatasource object to make queries
     */
    private NewSavedDatasource dataSource;

    /**
     * ListView attribute to manage the channel list
     */
    private ListView savedNewsList;

    /**
     * SavedNewsListAdapter object to process saved news in the app
     */
    private SavedNewsListAdapter adapter;

    /**
     * List to get the saved news via sqlite
     */
    private List<NewSaved> newsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.savednews_activity);
        initElements();
    }

    @SuppressLint("NewApi")
    public void initElements() {

        actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.new_saved));
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        speech = new Speech(this);

        context = getApplicationContext();
        channelUIObj = (ChannelUI) getIntent().getSerializableExtra("channelObj");

        dataSource = new NewSavedDatasource(this);
        dataSource.open();
        newsList = dataSource.getAllNews();

        savedNewsList = (ListView) findViewById(R.id.savedNewsList);

        adapter = new SavedNewsListAdapter(this, context, newsList);
        savedNewsList.setAdapter(adapter);

        savedNewsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                NewSaved n = newsList.get((int) id);
                speech.speakAfterCheckingForTTS(n.getName() + ". " + n.getDescription());
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                Intent intent = new Intent(SavedNewsActivity.this, LectorNoticias.class);
                intent.putExtra("channelObj", channelUIObj);
                startActivity(intent);
                return true;

        }

        return true;
    }

}
