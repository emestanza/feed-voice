package com.voicefeeds;

import android.util.Log;
import com.voicefeeds.beans.HttpVFResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by W&L on 28/10/2014.
 */
public class VFHttpRequest {

    private HttpResponse response;
    private HttpPost httpPost;
    private HttpClient httpClient;
    private String responseBody="";
    private HttpEntity entityResponse;

    public HttpVFResponse makePostRequest(String url, List<NameValuePair> params) {

        httpClient = new DefaultHttpClient();
        // replace with your url
        httpPost = new HttpPost(url);

        //Encoding POST data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }

        //making POST request.
        try {
            response = httpClient.execute(httpPost);
            entityResponse = response.getEntity();
            responseBody = EntityUtils.toString(entityResponse);
            // write response to log
            Log.d("Http Post Response:", response.toString());
        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }

        return new HttpVFResponse(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), responseBody);
    }
}
