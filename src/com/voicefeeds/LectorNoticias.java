package com.voicefeeds;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import android.graphics.Bitmap;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Xml;
import com.voicefeeds.beans.ChannelUI;
import com.voicefeeds.beans.NewSaved;
import com.voicefeeds.utils.AppConfig;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class LectorNoticias extends ActionBarActivity {

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * application context to be used during the activity
     */
    private Context context;

    /**
     * ListView to be used to list the latest news from the feed
     */
    private ListView listView;

    /**
     * ArrayList to be used to add all the news to be processed by the adapter
     */
    private ArrayList<Noticia> noticias;

    /**
     * Adapter of the application, it is going to be used to show the news
     */
    private NoticiasAdapter noticiasAdapter;

    /**
     * Async task to process the feed in background
     */
    private TareaDescarga tarea;

    /**
     * Speech object to set the new spoken
     */
    private Speech speech;

    /**
     * ChannelUI object to get the previous values selected by the user
     */
    private ChannelUI channelUIObj;

    /**
     * Bitmap object to use to get the image of the newspaper
     */
    private Bitmap imageBitmap;

    /**
     * NewSavedDatasource object to make queries
     */
    private NewSavedDatasource dataSource;

    /**
     * LoadImage object to get an image
     */
    private LoadImage loadImgObj;

    /**
     * List to get the news of the selected newspaper
     */
    private List<NewSaved> newsList;

    /**
     * onCreate method override
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initElements();
    }

    /**
     * onStop method override
     */
    @Override
    public void onStop() {
        speech.stop();
        super.onStop();
    }

    /**
     * onCreateOptionsMenu method override
     * @param m
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.menu, m);
        return super.onCreateOptionsMenu(m);
    }

    /**
     * onOptionsItemSelected method override
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;

        switch (item.getItemId()) {

            case R.id.item01:
                speech.stop();
                break;

            case R.id.item02:
                noticias = new ArrayList<Noticia>();
                noticiasAdapter = new NoticiasAdapter(context, R.layout.fila, noticias, newsList);
                lanzaDescargaDeNoticias();
                break;

            case R.id.item03:
                AlertDialog.Builder ab = new AlertDialog.Builder(LectorNoticias.this);
                ab.setTitle(R.string.acerca_de_camon);
                ab.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
                ab.setMessage(R.string.licencia);
                ab.setPositiveButton(R.string.aceptar, null);
                ab.show();
                break;

            case android.R.id.home:
                intent = new Intent(LectorNoticias.this, CategoryActivity.class);
                intent.putExtra("channelObj", channelUIObj);
                startActivity(intent);
                return true;

            case R.id.itemSaved:
                intent = new Intent(LectorNoticias.this, SavedNewsActivity.class);
                intent.putExtra("channelObj", channelUIObj);

                startActivity(intent);
                return true;
        }
        return true;
    }

    /**
     * Method to read the feed from url
     */
    void lanzaDescargaDeNoticias() {
        try {
            tarea = new TareaDescarga();
            String[] urls = channelUIObj.getChannelUrls();
            tarea.execute(new URL(urls[channelUIObj.getCategoryIndexSelected()]));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Private class to read a xml feed
     */
    private class TareaDescarga extends AsyncTask<URL, String, List<Noticia>> {

        ArrayList<Noticia> noticiasDescargadas;

        ProgressDialog progressDialog;

        boolean error = false;

        // We don't use namespaces
        private final String ns = null;

        public TareaDescarga() {
        }

        private ArrayList<Noticia> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
            ArrayList<Noticia> entries = new ArrayList();

            parser.require(XmlPullParser.START_TAG, ns, "rss");
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, ns, "channel");

            while (parser.next() != XmlPullParser.END_TAG) {

                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();

                if (name.equals("item")) {
                    Noticia notObj = readEntry(parser);
                    entries.add(notObj);

                } else {
                    skip(parser);
                }
            }
            return entries;
        }


        // Processes title tags in the feed.
        private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "title");
            String title = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "title");
            return title;
        }

        // Processes link tags in the feed.
        private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
            String link = "";
            parser.require(XmlPullParser.START_TAG, ns, "link");
            link = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "link");
            return link;
        }

        // Processes link tags in the feed.
        private String readPubDate(XmlPullParser parser) throws IOException, XmlPullParserException {
            String link = "";
            parser.require(XmlPullParser.START_TAG, ns, "pubDate");
            link = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "pubDate");
            return link;
        }

        // Processes summary tags in the feed.
        private Spanned readSummary(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "description");
            Spanned texto = Html.fromHtml(readText(parser), new ImageGetter(), null);
            parser.require(XmlPullParser.END_TAG, ns, "description");
            return texto;
        }

        // For the tags title and summary, extracts their text values.
        private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
            String result = "";
            if (parser.next() == XmlPullParser.TEXT) {
                result = parser.getText();
                parser.nextTag();
            }
            return result;
        }

        // Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
        // to their respective "read" methods for processing. Otherwise, skips the tag.
        private Noticia readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {

            parser.require(XmlPullParser.START_TAG, ns, "item");
            Noticia newObj = new Noticia();

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();

                if (name.equals("title")) {
                    newObj.setTitulo(readTitle(parser));
                } else if (name.equals("description")) {
                    newObj.setDescripcion(readSummary(parser));
                } else if (name.equals("link")) {
                    newObj.setLink(readLink(parser));
                } else if (name.equals("pubDate")) {
                    newObj.setFecha(readPubDate(parser));
                } else {
                    skip(parser);
                }
            }

            return newObj;
        }

        private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                throw new IllegalStateException();
            }
            int depth = 1;
            while (depth != 0) {
                switch (parser.next()) {
                    case XmlPullParser.END_TAG:
                        depth--;
                        break;
                    case XmlPullParser.START_TAG:
                        depth++;
                        break;
                }
            }
        }

        @Override
        protected List<Noticia> doInBackground(URL... params) {
            try {

                noticiasDescargadas = new ArrayList<Noticia>();

                for (int i = 0; i < params.length; i++) {
                    URL url = params[i];

                    XmlPullParser parser = Xml.newPullParser();
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(url.openStream(), null);
                    parser.nextTag();
                    noticiasDescargadas = readFeed(parser);

                }

                Collections.sort(noticiasDescargadas, new Comparator<Noticia>() {

                    @Override
                    public int compare(Noticia lhs, Noticia rhs) {
                        Date fecha1 = null;
                        Date fecha2 = null;

                        try {
                             fecha1 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z").parse(lhs.getFecha());
                             fecha2 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z").parse(rhs.getFecha());

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (fecha1 == null || fecha2 == null)
                            return 0;

                        return fecha2.compareTo(fecha1);
                    }
                });

            } catch (Exception e) {
                Log.e("Net", "Error in network call", e);
                error = true;
            }
            return noticiasDescargadas;
        }

        String imageSource = null;

        class ImageGetter implements Html.ImageGetter {
            public Drawable getDrawable(String source) {
                imageSource = source;
                return new BitmapDrawable();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            noticiasAdapter.clear();
            progressDialog = ProgressDialog.show(LectorNoticias.this, getString(R.string.espere), getString(R.string.descargandonoticias), true, true);
            progressDialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    tarea.cancel(true);
                }
            });
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            noticiasAdapter.clear();
            noticiasDescargadas = new ArrayList<Noticia>();
        }

        @Override
        protected void onProgressUpdate(String... progreso) {
            super.onProgressUpdate(progreso);
            progressDialog.setMessage(progreso[0]);
            noticiasAdapter.notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(List<Noticia> result) {
            super.onPostExecute(result);

            if (noticiasDescargadas.size() == 0) {
                progressDialog.dismiss();
                Toast.makeText(context, R.string.noNews, Toast.LENGTH_LONG).show();
            } else {

                for (Noticia n : noticiasDescargadas) {
                    noticiasAdapter.add(n);
                }

                noticiasAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
                Log.d("LectorNoticias.newsListedOK", "success");
            }

            if (error) {
                Toast.makeText(context, R.string.errordered, Toast.LENGTH_LONG).show();
                Log.d("LectorNoticias.newsListedFailed", "failed");
            }
        }
    }

    /**
     * Initialize all the elements to be used by the activity
     */
    @SuppressLint("NewApi")
    public void initElements() {
        dataSource = new NewSavedDatasource(this);
        dataSource.open();

        newsList = dataSource.getAllNews();

        context = getApplicationContext();
        channelUIObj = (ChannelUI) getIntent().getSerializableExtra("channelObj");

        try {
            loadImgObj = new LoadImage();
            imageBitmap = loadImgObj.execute(AppConfig.NEWSPAPER_DIRECTORY+"/"+channelUIObj.getChannelImage()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        };

        actionBar = getSupportActionBar();
        actionBar.setTitle(channelUIObj.getChannelName() + " - " + channelUIObj.getChannelCategories()[channelUIObj.getCategoryIndexSelected()]);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Drawable drawable=new BitmapDrawable(getResources(), imageBitmap);

        actionBar.setLogo(drawable);

        @SuppressWarnings("deprecation")
        final ArrayList<Noticia> data = (ArrayList<Noticia>) getLastNonConfigurationInstance();

        if (data == null) {
            noticias = new ArrayList<Noticia>();
            noticiasAdapter = new NoticiasAdapter(context, R.layout.fila, noticias, newsList);
            lanzaDescargaDeNoticias();
        } else {
            noticias = data;
            noticiasAdapter = new NoticiasAdapter(context, R.layout.fila, noticias, newsList);
        }

        speech = new Speech(this);

        listView = (ListView) findViewById(R.id.ListView01);
        listView.setAdapter(noticiasAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                Noticia n = noticias.get((int) id);
                speech.speakAfterCheckingForTTS(n.getTitulo()+". "+n.getDescripcion());

            }

        });

    }

}