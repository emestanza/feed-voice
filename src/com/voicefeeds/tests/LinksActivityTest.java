package com.voicefeeds.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import com.robotium.solo.Solo;
import com.voicefeeds.CategoryActivity;
import com.voicefeeds.ChannelListActivity;
import com.voicefeeds.R;
import junit.framework.Assert;

@SuppressLint("NewApi")
public class LinksActivityTest extends ActivityInstrumentationTestCase2<ChannelListActivity> {

    private Solo solo;

    public LinksActivityTest() {
        super(ChannelListActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    public void testLinksList() throws Exception {

        for (int j = 1;j<=3;j++){

            solo.clickInList(j);
            solo.waitForActivity(CategoryActivity.class);
            solo.clickOnButton("Verificar Voz");
            solo.sleep(5000);
            Assert.assertTrue(solo.waitForLogMessage("CategoryActivity.TTSOK", 4000));
            View view1 = null;

            checkingNews();

            for (int i = 1; i<=6;i++){

                solo.clickOnActionBarHomeButton();
                view1 = solo.getView(Spinner.class, 0);
                solo.clickOnView(view1);
                solo.scrollToTop();
                solo.clickOnView(solo.getView(TextView.class, i));
                checkingNews();
            }

            solo.clickOnActionBarHomeButton();
            solo.clickOnActionBarHomeButton();
        }

    }

    public void checkingNews(){

        solo.clickOnActionBarItem(R.id.acceptCategoryBtn);
        Assert.assertTrue(solo.waitForLogMessage("LectorNoticias.newsListedOK", 4000));
        solo.scrollDown();
        solo.sleep(2000);
        solo.scrollUp();
        solo.clickInList(1);
        solo.sleep(15000);
        solo.clickInList(2);
        solo.sleep(15000);
        solo.clickInList(3);
        solo.sleep(15000);

    }

}