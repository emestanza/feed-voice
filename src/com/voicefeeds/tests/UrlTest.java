package com.voicefeeds.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import com.robotium.solo.Solo;
import com.voicefeeds.CategoryActivity;
import com.voicefeeds.ChannelListActivity;
import com.voicefeeds.R;
import junit.framework.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@SuppressLint("NewApi")
public class UrlTest extends ActivityInstrumentationTestCase2<ChannelListActivity> {

    private Solo solo;

    public UrlTest() {
        super(ChannelListActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    public void testUrlsList() throws Exception {

      String urls[]=  this.getInstrumentation().getTargetContext().getApplicationContext().getResources().getStringArray(R.array.elComercioUrlArray);

        for (int i =0 ;i<urls.length;i++){

            try {
                URL url = new URL(urls[i]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                Log.i("URLTesting",urls[i]);
                Assert.assertTrue(urls[i], con.getResponseCode() == 200);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        urls=  this.getInstrumentation().getTargetContext().getApplicationContext().getResources().getStringArray(R.array.peru21UrlArray);

        for (int i =0 ;i<urls.length;i++){

            try {
                URL url = new URL(urls[i]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                Log.i("URLTesting",urls[i]);
                Assert.assertTrue(urls[i], con.getResponseCode() == 200);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        urls=  this.getInstrumentation().getTargetContext().getApplicationContext().getResources().getStringArray(R.array.gestionUrlArray);

        for (int i =0 ;i<urls.length;i++){

            try {
                URL url = new URL(urls[i]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                Log.i("URLTesting",urls[i]);
                Assert.assertTrue(urls[i], con.getResponseCode() == 200);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}