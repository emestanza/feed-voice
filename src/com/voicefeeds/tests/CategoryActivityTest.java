package com.voicefeeds.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import com.robotium.solo.Solo;
import com.voicefeeds.CategoryActivity;
import com.voicefeeds.ChannelListActivity;
import com.voicefeeds.R;
import junit.framework.Assert;

@SuppressLint("NewApi")
public class CategoryActivityTest extends ActivityInstrumentationTestCase2<ChannelListActivity> {

    private Solo solo;

    public CategoryActivityTest() {
        super(ChannelListActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }


    public void testCategoryList() throws Exception {

        solo.clickInList(1);
        solo.waitForActivity(CategoryActivity.class);
        Assert.assertTrue(solo.waitForText("Perú 21", 1, 1000));
        Spinner aux = (Spinner) solo.getView(R.id.categorySpinner);
        Assert.assertTrue(aux.getCount() == 11);
        Assert.assertTrue(solo.waitForText("Actualidad", 1, 500));

        View view1 = solo.getView(Spinner.class, 0);
        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 1));

        Assert.assertTrue(solo.waitForText("Deporte", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 2)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Economía", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 3)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Mundo", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 4)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Vida 21", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 5)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Espectáculos", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 6)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Mis Finanzas", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 7)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Reportuit", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 8)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Emprendedores", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 9)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Tecnología", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        solo.clickOnView(solo.getView(TextView.class, 10)); // select the 3rd item in the spinner

        Assert.assertTrue(solo.waitForText("Videojuegos", 1, 500));


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        solo.clickOnActionBarHomeButton();
        solo.clickInList(2);
        Assert.assertTrue(solo.waitForText("El Comercio", 1 , 1000));

        aux = (Spinner) solo.getView(R.id.categorySpinner);

        Assert.assertTrue(aux.getCount() == 24);

        Assert.assertTrue(solo.waitForText("Portada", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 1));
        Assert.assertTrue(solo.waitForText("Política", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 2));
        Assert.assertTrue(solo.waitForText("Lima", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 3));
        Assert.assertTrue(solo.waitForText("Perú", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 4));
        Assert.assertTrue(solo.waitForText("Ciencias", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 5));
        Assert.assertTrue(solo.waitForText("Tecnología", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 6));
        Assert.assertTrue(solo.waitForText("Mundo", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 7));
        Assert.assertTrue(solo.waitForText("Economía", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 8));
        Assert.assertTrue(solo.waitForText("Luces", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 9));
        Assert.assertTrue(solo.waitForText("TV +", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 10));
        Assert.assertTrue(solo.waitForText("Deporte Total", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 11));
        Assert.assertTrue(solo.waitForText("Insolito", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 12));
        Assert.assertTrue(solo.waitForText("Repotube", 1, 500));

        /*
        solo.clickOnView(view1);
        solo.scrollToBottom(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 13));
        Assert.assertTrue(solo.waitForText("Gastronomía", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 14));
        Assert.assertTrue(solo.waitForText("Vamos", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 15));
        Assert.assertTrue(solo.waitForText("Viú", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 16));
        Assert.assertTrue(solo.waitForText("Ruedas y Tuercas", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 17));
        Assert.assertTrue(solo.waitForText("Casa y más", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 18));
        Assert.assertTrue(solo.waitForText("Opinión", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 19));
        Assert.assertTrue(solo.waitForText("El Dominical", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 20));
        Assert.assertTrue(solo.waitForText("Art Lima", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 21));
        Assert.assertTrue(solo.waitForText("Lifweek", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 22));
        Assert.assertTrue(solo.waitForText("Perumin", 1, 1000));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 23));
        Assert.assertTrue(solo.waitForText("Hola", 1, 1000));

*/

        solo.clickOnActionBarHomeButton();
        solo.clickInList(3);
        Assert.assertTrue(solo.waitForText("Gestión", 1 , 1000));

        aux = (Spinner) solo.getView(R.id.categorySpinner);

        Assert.assertTrue(aux.getCount() == 9);
        Assert.assertTrue(solo.waitForText("Economía", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 1));
        Assert.assertTrue(solo.waitForText("Mercados", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 2));
        Assert.assertTrue(solo.waitForText("Inmobiliaria", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 3));
        Assert.assertTrue(solo.waitForText("Política", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 4));
        Assert.assertTrue(solo.waitForText("Tendencias", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 5));
        Assert.assertTrue(solo.waitForText("Empresas", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 6));
        Assert.assertTrue(solo.waitForText("Tu Dinero", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 7));
        Assert.assertTrue(solo.waitForText("Empleo y Management", 1, 500));

        solo.clickOnView(view1);
        solo.scrollToTop(); // I put this in here so that it always keeps the list at start
        // select the 2nd item in the spinner
        solo.clickOnView(solo.getView(TextView.class, 8));
        Assert.assertTrue(solo.waitForText("Tecnología", 1, 500));

    }

}