package com.voicefeeds.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import com.robotium.solo.Solo;
import com.voicefeeds.ChannelListActivity;
import junit.framework.Assert;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.cluby.InitActivityTest \
 * com.cluby.tests/android.test.InstrumentationTestRunner
 */
@SuppressLint("NewApi")
public class InitActivityTest extends ActivityInstrumentationTestCase2<ChannelListActivity> {

    private Solo solo;

    public InitActivityTest() {
        super(ChannelListActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }


    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }


    public void testChannelList() throws Exception {

        solo.clickInList(1);
        Assert.assertTrue(solo.waitForText("Perú 21", 1, 2000));
        solo.clickOnActionBarHomeButton();
        solo.clickInList(2);
        Assert.assertTrue(solo.waitForText("El Comercio", 1 , 2000));
        solo.clickOnActionBarHomeButton();
        solo.clickInList(3);
        Assert.assertTrue(solo.waitForText("Gestión", 1 , 2000));
        solo.clickOnActionBarHomeButton();


        for (int i=0;i<10;i++){

            solo.clickInList(1);
            Assert.assertTrue(solo.waitForText("Perú 21", 1 , 1000));
            solo.clickOnActionBarHomeButton();

        }

        for (int i=0;i<10;i++){

            solo.clickInList(2);
            Assert.assertTrue(solo.waitForText("El Comercio", 1 , 1000));
            solo.clickOnActionBarHomeButton();

        }

        for (int i=0;i<10;i++){

            solo.clickInList(3);
            Assert.assertTrue(solo.waitForText("Gestión", 1 , 1000));
            solo.clickOnActionBarHomeButton();

        }
    }
}