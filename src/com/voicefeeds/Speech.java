package com.voicefeeds;

import java.util.Locale;
import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.Engine;
import android.speech.tts.TextToSpeech.OnInitListener;

public class Speech {
	private Activity activity;
	private TextToSpeech tts;
	private static final int TTS_DATA_CHECK = 1;
	private String text;
	private boolean isTTSinstalled = false;
	private boolean isTTSinitialized = false;
	
	
	public Speech(Activity activity){

		this.activity = activity;
        tts = new TextToSpeech(activity, new OnInitListener() {
            public void onInit(int status) {

                String aaa="";

                if (status == TextToSpeech.SUCCESS) {
                    Locale loc = new Locale("es","","");
                    if (tts.isLanguageAvailable(loc) >= TextToSpeech.LANG_AVAILABLE)
                        tts.setLanguage(loc);
                    tts.setPitch(0.8f);
                    tts.setSpeechRate(1.1f);
                    isTTSinitialized = true;
                }
            }
        });
    }
	
	
	public void speakAfterCheckingForTTS(String txt){
		this.text = txt;
		if(isTTSinitialized){
			tts.speak(txt, TextToSpeech.QUEUE_ADD, null);
		}else{
			Intent intent = new Intent(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
			activity.startActivityForResult(intent, TTS_DATA_CHECK);
		}
		
	}
	
	
	public void speakWithoutCheckingForTTS(String txt){

		tts = new TextToSpeech(activity, new OnInitListener() {
			public void onInit(int status) {
				if (status == TextToSpeech.SUCCESS) {
                    Locale loc = new Locale("es","","");
					if (tts.isLanguageAvailable(loc) >= TextToSpeech.LANG_AVAILABLE)
						tts.setLanguage(loc);
					tts.setPitch(0.8f);
					tts.setSpeechRate(1.1f);
					isTTSinitialized = true;

					//Speak this:
					tts.speak(text, TextToSpeech.QUEUE_ADD, null);
				}
			}
		});			
	}

    
    public void installOrSpeak(int requestCode, int resultCode, boolean test){

        String aux = "";

        if (test) text = "Esta es una prueba para verificar el funcionamiento de la voz";

    	if(requestCode == TTS_DATA_CHECK){
			if (resultCode == Engine.CHECK_VOICE_DATA_PASS) {
				isTTSinstalled = true;
				speakWithoutCheckingForTTS(text);
			} else {
				Intent installVoice = new Intent(Engine.ACTION_INSTALL_TTS_DATA);
				activity.startActivity(installVoice);
			}
    	}    	
    }
	
	
    public void stop(){
		if (tts != null) {
			tts.stop();
			tts.shutdown();
			isTTSinitialized = false;
		}
    }


	public boolean isTTSinstalled() {
		return isTTSinstalled;
	}
       

}
